#!/usr/bin/env bash
#!/bin/bash
# Ask the user for login details
echo "================= Database source ================="
read -p "host (server): " source_host;
source_host=${source_host:-"server"};

read -p "user (postgres): " source_user;
source_user=${source_user:-"postgres"};

read -p "database (postgres): " source_database;
source_database=${source_database:-"postgres"}

read -p "${source_user} password: " source_password


echo
echo
echo "================== SSH SERVER ==================="
read -p "user (brainsoft): " ssh_user;
ssh_user=${ssh_user:-"brainsoft"};
read -sp "${ssh_user} password: " ssh_password;

echo
echo
echo "==================== LOCAL ====================="

read -p "C/Clone | D/Dump (C/d): " clone_action
clone_action=${clone_action:-"C"};

if [[ "${clone_action}" == "" ]]; then
    echo
    read -p "database (clone_db): " clone_database;
    clone_database=${clone_database:-"clone_db"};

    read -p "user (clone_sa): " clone_user;
    clone_user=${clone_user:-"clone_sa"};
    read -p "password for ${clone_user}: " clone_password;

    echo
    read -p "clear clone (S/n): " clone_clear;
    clone_clear=${clone_clear:-"S"};

    read -p "default schemes: " clone_scheme
    read -p "default timezone (Africa/Sao_Tome): " clone_timezone
    clone_timezone=${clone_timezone:-"Africa/Sao_Tome"}
fi;

echo
echo
file_default="${clone_database}.${source_database}.clone.database.sql";
read -p "File name: " file_name
file_name=${file_name:-"${file_default}"}



echo "Clonando a base de dados para o ficheiro ${file_name};"
PGPASSWORD="${source_password}" pg_dump -h ${source_host} -U ${source_user} -d ${source_database} -cOv --if-exist -f ${file_name}


if [[ "${clone_action}" == "" ]]; then

    echo "Importando a base de dados ${source_database} para ${clone_database} usando o ficheiro ${file_name};"

    if [[ "${clone_clear}" == "S" ]]; then
        echo "Cleaning clone..."
        PGPASSWORD="${clone_superuser_password}" psql postgres ${clone_superuser} -h ${clone_host} -c "drop database if exists ${clone_database};"
        PGPASSWORD="${clone_superuser_password}" psql postgres ${clone_superuser} -h ${clone_host} -c "drop user if exists ${clone_user};"

        echo "Recreating clone..."
        PGPASSWORD="${clone_superuser_password}" psql postgres ${clone_superuser} -h ${clone_host} -c "create user ${clone_user};"
        PGPASSWORD="${clone_superuser_password}" psql postgres ${clone_superuser} -h ${clone_host} -c "create database ${clone_database} with owner ${clone_user};"
        PGPASSWORD="${clone_superuser_password}" psql postgres ${clone_superuser} -h ${clone_host} -c "alter user ${clone_user} with password '${clone_password}';"
    fi;

    echo "Settings configurations..."
    if [[ "${clone_scheme}" != "" ]]; then
        PGPASSWORD="${clone_superuser_password}" psql postgres postgres -h ${clone_host} -c "alter database ${clone_database} set search_path to ${clone_scheme};"
    fi;

    PGPASSWORD="${clone_superuser_password}" psql postgres postgres -h ${clone_host} -c "alter database ${clone_database} set timezone to '${clone_timezone}';"


    echo "Importing clone..."
    PGPASSWORD="${clone_password}" psql ${clone_database} ${clone_user} -h ${clone_host} < ${file_name};

    echo
    echo
    echo "Clone complete!"
fi;