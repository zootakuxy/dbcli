#!/usr/bin/env bash
#!/bin/bash
# Ask the user for login details
echo "================= Database source ================="
read -p "host (::0): " source_host;
source_host=${source_host:-"::0"};

source_user=${source_user:-"datalib_sa"};
source_database=${source_database:-"datalib_db"}

read -sp "${source_user} password: " source_password

echo
echo
echo "================= Database clone =================="

read -p "host (::0): " clone_host;
clone_host=${clone_host:-"::0"};
read -p "superuser (postgres): " clone_superuser;
clone_superuser=${clone_superuser:-"postgres"};
read -p "${clone_superuser} password: " clone_superuser_password;

echo
read -p "database (clone_db): " clone_database;
clone_database=${clone_database:-"clone_db"};

read -p "user (clone_sa): " clone_user;
clone_user=${clone_user:-"clone_sa"};

read -p "password for ${clone_user}: " clone_password;

echo
read -p "clear clone (S/n): " clone_clear;
clone_clear=${clone_clear:-"S"};
read -p "default schemes: " clone_scheme
read -p "default timezone (Africa/Sao_Tome): " clone_timezone
clone_timezone=${clone_timezone:-"Africa/Sao_Tome"}

echo
echo
file_name="${clone_database}.${source_database}.clone.database.sql";

echo "Clonando a base de dados para o ficheiro ${file_name};"
PGPASSWORD="${source_password}" pg_dump -h ${source_host} -U ${source_user} -d ${source_database} -cOv --if-exist -f ${file_name}



if [[ "${clone_clear}" == "S" ]]; then
    echo "Cleaning clone..."
    PGPASSWORD="${clone_superuser_password}" psql postgres ${clone_superuser} -h ${clone_host} -c "drop database if exists ${clone_database};"
    PGPASSWORD="${clone_superuser_password}" psql postgres ${clone_superuser} -h ${clone_host} -c "drop user if exists ${clone_user};"

    echo "Recreating clone..."
    PGPASSWORD="${clone_superuser_password}" psql postgres ${clone_superuser} -h ${clone_host} -c "create user ${clone_user};"
    PGPASSWORD="${clone_superuser_password}" psql postgres ${clone_superuser} -h ${clone_host} -c "create database ${clone_database} with owner ${clone_user};"
fi;

echo "Settings configurations..."
if [[ "${clone_scheme}" != "" ]]; then
    PGPASSWORD="${clone_superuser_password}" psql postgres postgres -h ${clone_host} -c "alter database ${clone_database} set search_path to ${clone_scheme};"
fi;

PGPASSWORD="${clone_superuser_password}" psql postgres ${clone_superuser} -h ${clone_host} -c "alter user ${clone_user} with password '${clone_password}';"
PGPASSWORD="${clone_superuser_password}" psql postgres postgres -h ${clone_host} -c "alter database ${clone_database} set timezone to '${clone_timezone}';"

echo "Importando a base de dados ${source_database} para ${clone_database} com ${clone_user} usando o ficheiro ${file_name};"
PGPASSWORD="${clone_password}" psql ${clone_database} ${clone_user} -h ${clone_host} < ${file_name};

rm -rf ${file_name};

echo
echo
echo "Clone complete!"
