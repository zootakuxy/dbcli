#!/usr/bin/env bash

#!/usr/bin/env bash


# Incluir as dependencias
source "../lib/df-manager.sh";
source "../lib/str.sh"

help_text="
  [Connection]
    -h, --host HOST                 Host name of database (Required)
    -u, --user USER                 User of connection (Required)
    -d, --database DB-NAME          Database to dump (Required)
    -p, --port PORT-NUMBER          Number of port to connection default is 5432
    -w, --password PASSWORD         Password of user

  [Backup archives]
    -l, --location BACKUP-DIR       Directory to store backup files (Required)
    -a, --archives N-ARCHIVES       Number archives of backup to store default 30
    -f, --file FILE-NAME            Name of file to backup
                                    Default filename is \$(date +%y%m%d-%H%M%S).\${database}.backup
    -t, --tar                       Archive backup in tar mode default is archived in (.sql)

  [Database options]
    --if-exists                     Include if exists in backups
    -n, --schema SCHEMA             Specify schema of backup

  [Help]
    ?, --help                       Show help text
"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in

  ## Connection
    -h|--host)            host=${2};              shift; shift; ;;
    -u|--user)            user=${2};              shift; shift; ;;
    -d|--database)        database=${2};          shift; shift; ;;
    -w| --password)       password=${2};          shift; shift; ;;
    -p| --port)           port=${2};              shift; shift; ;;

  ## Backup archive
    -l|--location)        location=${2};          shift; shift; ;;
    -a|--archives)        archives=${2};          shift; shift; ;;
    -f|--file)            file=${2};              shift; shift; ;;
    -t|--tar)             format="-F t";          shift; shift; ;;

  ## Database options
    --if-exist)     if_exists="--if-exists";      shift; shift; ;;
    -n|--schema)    schema="${schema}$ -n ${2}";  shift; shift; ;;

  ## Help
    ?|--help)       help="YES";     shift; shift; ;;

  # unknown option
    *)
    POSITIONAL+=("$1") # save it in an array for later
    echo "${1} option not recognized"
    exit;
    shift;
    shift;
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ "${help}" == "YES" ]]; then
  echo "${help_text}";
  exit;
fi;


database_host=${host};
database_user=${user};
database_name=${database};
database_password=${password};
database_port=${port};


backup_directory=${location}
backup_max_files=${archives};
backup_filename=${file};
backup_format=${format}
backup_schema=${schema}
backup_if_exists=${if_exists}

DEFAULT_MAX_FILES=2
DEFAULT_FILE="$(date +%y%m%d-%H%M%S).${database_name}.backup"
DEFAULT_PORT=5432


backup_filename=${backup_filename:-"${DEFAULT_FILE}"};
backup_max_files=${backup_max_files:-"${DEFAULT_MAX_FILES}"};
database_port=${database_port:-"${DEFAULT_PORT}"};

if [[ "${backup_format}" ]]; then
  backup_filename="${backup_filename}.tar";
else
  backup_filename="${backup_filename}.sql"
fi;


if [[ -z "$( str_normalize ${backup_directory} )" ]]; then
  echo "Directorio base do backup não foi expecificado, operação abortada";
  exit;
fi;

if [[ -z "$( str_normalize ${database_name} )" ]]; then
  echo "Nome da base de dados não expecificada, operação abortada";
  echo "-h, --help                       Show help text";
  exit;
fi;

if [[ -z "$( str_normalize ${database_host} )" ]]; then
  echo "Host não expecificado, operação abortada";
   echo "-h, --help                       Show help text";
  exit;
fi;

if [[ -z "$( str_normalize ${database_user} )" ]]; then
  echo "Utilizador não expecificado, operação abortada";
   echo "-h, --help                       Show help text";
  exit;
fi;

## Normalizar o directorio de modo a ter (/) no final
backup_directory=$( df_directory_normalize "${backup_directory}" )

if [[ ! -d "${backup_directory}" ]]; then
  mkdir    "${backup_directory}";
fi;

backup_file="${backup_directory}${backup_filename}"

## Calcular a quantidade de ficheiros velhos para ser removidos
database_directory_files=$(ls ${backup_directory} | wc -l);
total_file_delete=$(echo "${database_directory_files} - ${backup_max_files} +1" | bc )


if [[ ${total_file_delete} -lt 0 ]]; then
    total_file_delete=0;
fi;


## Remover todos todos os arquivos
iterate=0;
for rm_file in ${backup_directory}.* ${backup_directory}*; do

  ## Quando iterar todos os ficheiros a ser eliminado então abortar o for

  if [[ ${iterate} -ge ${total_file_delete} ]]; then break; fi;

  # Saltar os directorio base assim como o directorio parrent
  if [[ "${rm_file}" == "${backup_directory}." ]]; then continue; fi;
  if [[ "${rm_file}" == "${backup_directory}.." ]]; then continue; fi;

  echo "removing ${rm_file}";
  rm -rf ${rm_file};
  iterate=$(echo "${iterate} + 1" | bc);
done


## Efetuar o backup da base de dados
echo "pg_dump $(date +'%Y-%m-%d %M:%M:%S') | host: ${database_host}, user: ${database_user}, database: ${database_name} "
PGPASSWORD="${database_password}" pg_dump -h ${database_host} -p ${database_port} \
  -U ${database_user} \
  -d ${database_name} \
  -cOv ${backup_format} \
  ${backup_schema} \
  ${backup_if_exists} \
  -f ${backup_file}


