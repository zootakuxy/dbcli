#!/usr/bin/env bash


# Incluir as dependencias
source "../lib/df-manager.sh";
source "../lib/str.sh"

help_text="
  [Connection]
    -p, --parameter VALUE           Description of parameter

  [Help]
    ?, --help                       Show help text
"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in

  ## Connection
    -p|--parameter)                   parameter=${2};               shift; shift; ;;

  ## Help
    ?|--help)                         help="YES";                   shift; shift; ;;

  # unknown option
    *)
    POSITIONAL+=("$1") # save it in an array for later
    echo "${1} option not recognized"
    exit;
    shift;
    shift;
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ "${help}" == "YES" ]]; then
  echo "${help_text}";
  exit;
fi;
