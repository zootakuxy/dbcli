#!/usr/bin/env bash

current_dir=${PWD}

# Obter o directorio da execusão do arquivo
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "${DIR}"

# Incluir as dependencias
source "../src/lib/df-manager.sh";
source "../src/lib/str.sh"

# Voltar para o directorio da chamada
cd ${current_dir}

help_text="
  [About]
                                          [*] Required parameter
  [SSH connection]
    -H, --ssh-host HOST-NAME              SSH host name [*]
    -P, --ssh-port PORT-NUMBER            SSH port
    -U, --ssh-user USER-NAME              SSH user name [*]
    -W, --ssh-password PASSWORD

  [RSync]
    -s, --source [SRC]                    Directory source based [*]
    -d, --destine [DEST]                  Directory destination (base on home) [*]
    -p, --progress                        Show progress
    --delete                              Delete file in destine dir when not exist in source dir


  [Help]
    ?, --help                             Show help text
"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in

  ## SSH connection
    -H|--ssh-host)                    sshHost=${2};               shift; shift; ;;
    -P|--ssh-port)                    sshPort=${2};               shift; shift; ;;
    -U|--ssh-user)                    sshUser=${2};               shift; shift; ;;
    -W|--ssh-password)                sshPassword=${2};           shift; shift; ;;

  ## RSync
    -s|--source)                      source=${2};                shift; shift; ;;
    -d|--destine)                     destine=${2};               shift; shift; ;;
    -p|--progress)                    progress="--progress";      shift; ;;
    --delete )                       delete="--delete";      shift; ;;


  ## Help
    ?|--help)                         help="YES";                   shift; ;;

  # unknown option
    *)
    POSITIONAL+=("$1") # save it in an array for later
    echo "${1} option not recognized"
    exit;
    shift;
    shift;
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ "${help}" == "YES" ]]; then
  echo "${help_text}";
  exit;
fi;

ssh_host="${sshHost}";
ssh_port="${sshPort}";
ssh_user="${sshUser}";
ssh_password="${sshPassword}"

rsync_source="${source}"
rsync_destine="${destine}"
rsync_progress="${progress}"
rsync_delete="${delete}"

sshpass -p "${ssh_password}" rsync -avu ${rsync_delete} ${rsync_progress} -e "ssh -p ${ssh_port}" "${rsync_source}" ${ssh_user}@${ssh_host}:${rsync_destine}

