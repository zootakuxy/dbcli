#!/usr/bin/env bash

DEFAULT_DATABASE_HOST="localhost"
DEFAULT_DATABASE_USER="postgres"
DEFAULT_MAX_FILES=30
DEFAULT_DATE_FORMAT="%Y-%m-%d-%H%M%S"
CALL_DIR=${PWD};
BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Incluir as dependencias
cd "${BIN_DIR}"
source "../src/lib/df-manager.sh";
source "../src/lib/str.sh"
cd ${CALL_DIR}

help_text="
  [SSH connection]
    -S, --ssh                     Use pg-dump-ssh
    -H, --ssh-host  HOST          Host of SSH
    -U, --ssh-user USER           User of SSH
    -W, --ssh-pass PASSWORD       Password of ssh user
    -P, --ssh-port PORT-NUMBER    SSH port
    -L, --ssh-location DIR        SSH backup dir location (based on user home)

  [Database Connection]
    -h, --host HOST               Host name of database default ${DEFAULT_DATABASE_HOST}
    -u, --user USER               User of connection default ${DEFAULT_DATABASE_USER}
    -d, --database DB-NAME        Database to dump (Required)
    -p, --port PORT-NUMBER        Number of port to connection
    -w, --password PASSWORD       Password of database user


  [Backup archives]
    -l, --location BACKUP-DIR     Directory to store backup files (Required)
    -a, --archives N-ARCHIVES     Number archives of backup to store default ${DEFAULT_MAX_FILES}
    --date-format DATE-FORMAT     Format of data in file default is YYYY-MMDDHHMISS
                                  Default filename is \$(date +%y%m%d-%H%M%S).\${database}.backup
    -t, --tar                     Archive backup in tar mode default is archived in (.sql)
    -v, --verbose                 Verbose mode

  [Database options]
    -q, --quick                   Create a quick dump is auto include ( --clean, --no-owner, --if-exists, --verbose )
    -c, --clean                   Clean (drop) database objects before recreating
    --if-exists                   Include if exists in backups
    --O, --no-owner               skip restoration of object ownership in
    -n, --schema SCHEMA           Specify schema of backup

  [Help]
    ?, --help                     Show help text
"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in

  ## SSH Connection
    -S| --ssh)                ssh="YES";              shift; ;;
    -H| --ssh-host)           sshHost=${2};           shift; shift; ;;
    -P| --ssh-port)           sshPort="-P ${2}";           shift; shift; ;;
    -U| --ssh-user)           sshUser=${2};           shift; shift; ;;
    -W| --ssh-pass)           sshPassword=${2};       shift; shift; ;;

  ## Database Connection
    -h| --host)               host=${2};              shift; shift; ;;
    -u| --user)               user=${2};              shift; shift; ;;
    -d| --database)           database=${2};          shift; shift; ;;
    -p| --port)               port="--port=${2}";              shift; shift; ;;
    -w| --password)           pwd="${2}";         shift; shift; ;;

  ## Backup archive
    -l| --location)           location=${2};          shift; shift; ;;
    -a| --archives)           archives=${2};          shift; shift; ;;
    --date-format)            dateFormat=${2};            shift; shift; ;;
    -t| --tar)                pg_dump_ssh_format="-t"; pg_dump_format="-F t";          shift; ;;

  ## Database options
    -q| --quick)              quick="YES";            shift; ;;
    -c| --clean)              clean="--clean";        shift; ;;
    --if-exists)              if_exists="--if-exists";      shift; shift; ;;
    -O| --no-owner)           no_owner="--no-owner";  shift; ;;
    -v| --verbose)            verbose="--verbose";   shift; ;;
    -n| --schema)             schema="${schema} -n ${2}";  shift; shift; ;;

  ## Help
    ?| --help)       help="YES";     shift; shift; ;;

  # unknown option
    *)
    POSITIONAL+=("$1") # save it in an array for later
    echo "not recognized ${1}"
    exit;
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ "${help}" == "YES" ]]; then
  echo "${help_text}";
  exit;
fi;


db_host=${host};
db_user=${user};
db_name=${database};
db_password=${pwd};
db_port=${port};

ssh_user=${sshUser};
ssh_host=${sshHost};
ssh_password=${sshPassword}
ssh_port=${sshPort}

bk_no_owner=${no_owner}
bk_clean=${clean}
bk_verbose=${verbose}
bk_directory=${location}
bk_max_files=${archives};
bk_date_format=${dateFormat}
bk_schema=${schema}
bk_if_exists=${if_exists}

if [[ "${quick}" == "YES" ]]; then
  bk_if_exists="--if-exists";
  bk_no_owner="--no-owner";
  bk_clean="--clean";
  bk_verbose="--verbose";
fi;

# Set default values
bk_date_format=${bk_date_format:-"${DEFAULT_DATE_FORMAT}"};
date_file="$(date +${bk_date_format}).${db_name}.backup.database"
bk_filename=${bk_filename:-"${date_file}"};
bk_max_files=${bk_max_files:-"${DEFAULT_MAX_FILES}"};
db_host=${db_host:-"${DEFAULT_DATABASE_HOST}"};

if [[ "${pg_dump_format}" ]]; then
  bk_filename="${bk_filename}.tar";
else
  bk_filename="${bk_filename}.sql"
fi;

## Utilizar o cli pg-dump-ssh para efetuar o backup
if [[ "${ssh}" == "YES" ]]; then

  if [[ -z "$( str_normalize "${ssh_host}" )" ]]; then
    echo "SSH host not specified, aborted operation!";
    echo "-h, --help                       Show help text";
    exit
  fi;

  if [[ -z "$( str_normalize "${ssh_user}" )" ]]; then
    echo "SSH user not specified, aborted operation!";
    echo "-h, --help                       Show help text";
    exit
  fi;
fi;

if [[ -z "$( str_normalize "${bk_directory}" )" ]]; then
  echo "Local backup directory not specified, aborted operation!";
  echo "-h, --help                       Show help text";
  exit
fi;

if [[ -z "$( str_normalize "${db_name}" )" ]]; then
  echo "Name of database is not specified, aborted operation!";
  echo "-h, --help                       Show help text";
  exit;
fi;

if [[ -z "$( str_normalize "${db_user}" )" ]]; then
  echo "User of database is not specified, aborted operation!";
  echo "-h, --help                       Show help text";
  exit;
fi;

## Normalizar o directorio de modo a ter (/) no final
bk_directory=$( df_directory_normalize "${bk_directory}" )

## Criar o directorio base se não existir
if [[ ! -d "${bk_directory}" ]]; then mkdir -p "${bk_directory}"; fi;

bk_file="${bk_directory}${bk_filename}"

## Calcular a quantidade de ficheiros velhos para ser removidos
database_directory_files=$(ls "${bk_directory}" | wc -l);
total_file_delete=$(echo "${database_directory_files} - ${bk_max_files} +1" | bc );

if [[ ${total_file_delete} -lt 0 ]]; then
  total_file_delete=0;
fi;

## Remover todos todos os arquivos
cd "${bk_directory}"
iterate=0;
for rm_file in  ./.* ./*; do
  ## Quando iterar todos os ficheiros a ser eliminado então abortar o for

  if [[ ${iterate} -ge ${total_file_delete} ]]; then break; fi;

  # Saltar os directorio base assim como o directorio parrent
  if [[ "${rm_file}" == "." ]]; then continue; fi;
  if [[ "${rm_file}" == ".." ]]; then continue; fi;
  if [[ "${rm_file}" == "*" ]]; then continue; fi;

  if [[ "${rm_file}" == "./." ]]; then continue; fi;
  if [[ "${rm_file}" == "./.." ]]; then continue; fi;
  if [[ "${rm_file}" == "./*" ]]; then continue; fi;

  rm -rf ${rm_file};
  iterate=$(echo "${iterate} + 1" | bc);
done
cd ${CALL_DIR}


args_options=" ${db_port} \
  ${bk_clean} \
  ${bk_no_owner} \
  ${bk_verbose} \
  ${bk_if_exists} \
  ${bk_schema} \
";

if [[ "${ssh}" == "YES" ]]; then
  "${BIN_DIR}/pg-dump-ssh" -H "${ssh_host}"  -U "${ssh_user}" -W "${ssh_password}" -h "${db_host}" ${ssh_port} -u "${db_user}" -w "${db_password}" -d "${db_name}" -f "${bk_file}" ${pg_dump_ssh_format} ${args_options}
else
  PGPASSWORD="${db_password}" pg_dump -h "${db_host}" ${ssh_port} -U "${db_user}" -d "${db_name}" -f "${bk_file}" ${pg_dump_format} ${args_options}
fi;
