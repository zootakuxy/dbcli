#!/usr/bin/env bash

df_directory_normalize() {
  #Normalizar o normalizar o directory colocando a (/) no final
  dir=$1
  dir_length=${#dir}
  substr_start=$((${dir_length}-1 ))
  last_char=${dir:${substr_start}:${dir_length}}

  if [[ "${last_char}" != "/" ]]; then
      dir=${dir}/
  fi;
  ## Retornar o directorio normalizado
  echo "${dir}"
}

