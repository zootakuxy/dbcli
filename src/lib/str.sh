#!/usr/bin/env bash

str_no_space(){
  text=$1
  echo "${text// /}"
}

str_normalize(){
  text=$1
  echo "${text}" | sed -e 's/^[[:space:]]*//'
}