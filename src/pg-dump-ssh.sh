#!/usr/bin/env bash

DEFAULT_DATABASE_HOST="localhost"
DEFAULT_DATABASE_USER="postgres"

current_dir=${PWD}

# Obter o directorio da execusão do arquivo
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "${DIR}"

# Incluir as dependencias
source "../src/lib/df-manager.sh";
source "../src/lib/str.sh"

# Voltar para o directorio da chamada
cd ${current_dir}

help_text="
  [SSH connection]
    -H, --ssh-host  HOST             Host of SSH
    -U, --ssh-user USER              User of SSH
    -W, --ssh-pass PASSWORD          Password of ssh user
    -P, --ssh-port PORT-NUMBER       SSH port number to connection

  [Database Connection]
    -h, --host HOST                 Host name of database default ${DEFAULT_DATABASE_HOST}
    -u, --user USER                 User of connection default ${DEFAULT_DATABASE_USER}
    -d, --database DB-NAME          Database to dump (Required)
    -p, --port PORT-NUMBER          Number of port to connection
    -w, --password PASSWORD         Password of database user

  [Dump options]
    -q, --quick                     Create a quick dump is auto include ( --clean, --no-owner, --if-exists, --verbose )
    -f, --file FILE-NAME            Name of file to backup
    -t, --tar                       Archive backup in tar mode default is archived in plain text
    -c, --clean                     clean (drop) database objects before recreating
    --if-exists                     Include if exists in backups
    --O, --no-owner                 skip restoration of object ownership in
    -v, --verbose                   Verbose mode
    -n, --schema SCHEMA             Specify schema of backup
    -s, --schema-only SCHEMA        Specify schema of backup

  [Help]
    ?, -h, --help                       Show help text
"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in

  ## SSH Connection
    -H| --ssh-host)         sshHost=${2};           shift; shift; ;;
    -P| --ssh-port)         sshPort="-p ${2}";           shift; shift; ;;
    -U| --ssh-user)         sshUser=${2};           shift; shift; ;;
    -W| --ssh-pass)         sshPwd="${2}";       shift; shift; ;;
  ## Database Connection
    -h| --host)             host=${2};              shift; shift; ;;
    -u| --user)             user=${2};              shift; shift; ;;
    -d| --database)         database=${2};          shift; shift; ;;
    -p| --port)             port="--port=${2}";              shift; shift; ;;
    -w| --password)         pwd="PGPASSWORD=\"${2}\""; shift; shift; ;;

  ## Backup archive
    -f| --file)             file=${2};              shift; shift; ;;
    -t| --tar)              format="-F t";          shift; ;;


  ## Database options
    -q| --quick)            quick="YES";            shift; ;;
    -c| --clean)            clean="--clean";        shift; ;;
    --if-exists)             if_exists="--if-exists";shift; ;;
    -O| --no-owner)         no_owner="--no-owner";  shift; ;;
    -v| --verbose)          verbose="--verbose";   shift; ;;
    -n| --schema)           schema="${schema} -n ${2}";  shift; shift; ;;
    -s| --schema-only)           schema_only="${schema_only} -n ${2}";  shift; shift; ;;

  ## Help
    ?| -h| --help)       help="YES";     shift; shift; ;;

  # unknown option
    *)
    POSITIONAL+=("$1") # save it in an array for later
    echo "not recognized ${1}"
    exit;
    shift;
    shift;
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ "${help}" == "YES" ]]; then
  echo "${help_text}";
  exit;
fi;

db_host=${host};
db_user=${user};
db_name=${database};
db_password=${pwd};
db_port=${port};

ssh_user=${sshUser};
ssh_host=${sshHost};
ssh_password=${sshPwd}
ssh_port=${sshPort}

dump_filename="${file}";
dump_format="${format}"
dump_schema="${schema}"
dump_schema_only="${schema_only}"
dump_if_exists="${if_exists}"
dump_no_owner="${no_owner}"
dump_clean="${clean}"
dump_verbose="${verbose}"

if [[ "${quick}" == "YES" ]]; then
  dump_if_exists="--if-exists";
  dump_no_owner="--no-owner";
  dump_clean="--clean";
  dump_verbose="--verbose";
fi;



db_host=${db_host:-"${DEFAULT_DATABASE_HOST}"};
db_user=${db_user:-"${DEFAULT_DATABASE_USER}"};

if [[ -z "$( str_normalize "${db_name}" )" ]]; then
  echo "Name of database is not specified, aborted operation!";
  echo "-h, --help                       Show help text";
  exit;
fi;

if [[ -z "$( str_normalize "${ssh_host}" )" ]]; then
  echo "Host of server ssh is not specified, aborted operation!";
  echo "-h, --help                       Show help text";
  exit;
fi;

if [[ -z "$( str_normalize "${db_user}" )" ]]; then
  echo "User of database is not specified, aborted operation!";
  echo "-h, --help                       Show help text";
  exit;
fi;


dump_command="\
  ${db_password} \
   pg_dump -h \
   \"${db_host}\" \
   ${db_port} \
   -U \"${db_user}\" \
   -d \"${db_name}\" \
   ${dump_clean} \
   ${dump_no_owner} \
   ${dump_verbose} \
   ${dump_if_exists} \
   ${dump_schema} \
   ${dump_schema_only} \
   ${dump_format}
";

# Efetuaro backups

if [[ -z "${ssh_password}" ]]; then

  if [[ -z "$( str_normalize "${dump_filename}" )" ]]; then
    ssh ${ssh_port} ${ssh_user}@${ssh_host} " ${dump_command} "
  else
    ssh ${ssh_port} ${ssh_user}@${ssh_host}  "${dump_command}" > "${dump_filename}"
  fi;

else

  if [[ -z "$( str_normalize "${dump_filename}" )" ]]; then
    sshpass -p "${ssh_password}" ssh ${ssh_port} ${ssh_user}@${ssh_host} " ${dump_command} "
  else
    sshpass -p "${ssh_password}" ssh ${ssh_port} ${ssh_user}@${ssh_host}  "${dump_command}" > "${dump_filename}"
  fi;

fi;